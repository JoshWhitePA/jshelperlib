/* 
  Creator: Joshua White
  @version 0.01
  @requires jquery
 */


/* 
  Purpose: Check if string is numeric and greater than 0
  @param str String to be checked, must be a string!
  @return boolean Returns true if it is a normal integer
 */
function isNormalInteger(str) {
    var n = ~~Number(str);
    return !(String(n) === str && n >= 0);
}


/* 
  Purpose:Given ID to return selected item from dropdown
  @param String id
  @return selectedValue
 */
function getOptionSelectionByID(id){
  var selectElement = document.getElementById(id);
  var selectedValue = selectElement.options[selectElement.selectedIndex].value;
  return selectedValue;
}


/*
  Purpose: Validates all dropdowns in a general value in the correct values are known 
  @param id id of dropdown or select
  @param enumerateThis  enum of correct possible valuess
  @return boolean
 */
function validateSelection(id, enumerateThis){
   var userValue = getOptionSelectionByID(id);
   for(var singleEnum in enumerateThis){
      if(singleEnum == userValue){
        return true;
      }
   }
   return false;
}


function disableAllSibsButFirst(identifier){
  var $searchItems = $(identifier);
  var $first = $searchItems.first();
    $searchItems.prop('disabled',true);
    $first.prop('disabled',false);
}

function enablenextSib(theControl, identifier){
    var $currentItem = theControl;
    console.log($currentItem.closest(identifier));
    var $thing = $currentItem.parent().next().filter(identifier).first().eq(0);
    $thing.prop('disabled',false);
}

/* 
  Purpose: Reset all fields on a form except for buttons
  @param identifier Class of the form you want cleared
 */
function resetForm(identifier){
  //finds input that takes text and sets the value to "", excludes buttons
   $(identifier).find("input, textarea").not('input[type=button]').not('input[type=submit]').not('input[type=reset]').val("");
  //finds all selects and sets the first option as the selected option
   $(identifier).find('select :nth-child(1)').prop('selected', true);
  //unchecks all checkboxes
   $(identifier).find("input[type=checkbox]").prop('checked',false);
}


/* 
  Purpose: Create table rows based on data provided from a jagged array
          can be passed a table row class and/or a td class
          if you do not want those classes send an empty string
          
  Array Example:
      var tableData = new Array();
      tableData.push(["Trailer","Peterbilt","Duracel 700","2017",20000, "Red",false]);
    
  @param trClasses Holds class names of tr seperated by a space
  @param tdClasses Holds class names of td seperated by a space
  @return rows String of the populated table rows
 */
function buildTableRowsWithData(jaggedTableDataArr, trClasses, tdClasses ){
  var rows = "";
  if(trClasses.length == 0){
    trClasses = "";
  }else{
    trClasses = 'class="'+ trClasses + '"';
  }
  if(tdClasses.length == 0){
    tdClasses = "";
  }else{
    tdClasses = 'class="'+ tdClasses + '"';
  }
  
  var numRows = jaggedTableDataArr.length;
  for(var rowIDX = 0; rowIDX < numRows; rowIDX++){
    rows += '<tr '+trClasses+'>'; 

    for(var columnIDX = 0;columnIDX < jaggedTableDataArr[rowIDX].length; columnIDX++){
      rows += '<td '+ tdClasses +'>'+ jaggedTableDataArr[rowIDX][columnIDX] + '</td>';
    }
    rows += '</tr>';
  }
  return rows;
}


